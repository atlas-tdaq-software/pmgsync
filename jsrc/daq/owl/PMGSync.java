package daq.owl;

public class PMGSync {    
    private final static String libName = "pmgsync";
    
    static {
        try {
            System.loadLibrary(PMGSync.libName);
        }
        catch(UnsatisfiedLinkError ex) {
            System.err.println("Cannot load library \"" + PMGSync.libName + "\"");
            ex.printStackTrace();
        }
    }
    
    private PMGSync() {        
    }
    
    static public native void sync();
}