// File pmg_initSync.h
//
// This function is for use by external programs that want to synchronize
// with the pmg agent after they are created.
//
// A using program just include this file and calls the inline function
// when it has finished its initialization to signal it to the agent.
//
// Author: Pierre-yves Duval (CPPM)
// Creation: 24/11/99
//
//===================================================================
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <stdlib.h>

#include <pmg/pmg_initSync.h>
#include <pmg/pmg_syncMacros.h>
#include <daq_owl_PMGSync.h>

#include <system/File.h>
#include <ers/ers.h>

ERS_DECLARE_ISSUE( pmg, CantCreateSyncFile, "Cannot create sync file '" << name << "'", ((std::string)name ) )
ERS_DECLARE_ISSUE( pmg, EnvironmentNotSet, "Environment variable '" << PMG_SYNC_ENV_VAR_NAME << "' is not defined.", )

void pmg_initSync( )
{
    // get the name of the file to erase using the environment variable
    char * var = getenv( PMG_SYNC_ENV_VAR_NAME );

    if ( var ) {
	//test accessibility of directory and create it if not there
	System::File file( var );
	try {
	    file.ensure_path( S_IRWXU|S_IRWXG|S_IRWXO );
	    file.make_fifo( S_IRWXU|S_IRWXG|S_IRWXO );
	    ERS_DEBUG ( 1, "Sync file " << file.full_name() << " has been created.");
	}
	catch ( ers::Issue & ex ) {
	    ers::warning( pmg::CantCreateSyncFile( ERS_HERE, file.full_name(), ex ) );
	}
    } 
    else {
	ers::warning( pmg::CantCreateSyncFile( ERS_HERE, "null", pmg::EnvironmentNotSet( ERS_HERE ) ) );
    }
}

JNIEXPORT void JNICALL Java_daq_owl_PMGSync_sync
  (JNIEnv *, jclass) 
{
    pmg_initSync();
}
