// File pmg_initSync.h
//
// This function is for use by external programs that want to synchronize
// with the pmg agent after they are created.
//
// A using program just include this file and calls the inline function
// when it has finished its initialization to signal it to the agent.
//
// Author: Pierre-yves Duval (CPPM)
// Creation: 24/11/99
//
//===================================================================
#ifndef PMG_INITSYNC_H
#define PMG_INITSYNC_H


void pmg_initSync(void);


#endif //PMG_INITSYNC_H
